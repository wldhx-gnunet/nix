self: super: {
  haskellPackages = super.haskellPackages.override (oldArgs: {
    overrides =
      self.lib.composeExtensions (oldArgs.overrides or (_: _: {}))
        (hself: hsuper: {
          network-transport-tcp = self.haskell.lib.doJailbreak hsuper.network-transport-tcp;
          network-transport-tests = self.haskell.lib.doJailbreak hsuper.network-transport-tests;
          distributed-static = self.haskell.lib.doJailbreak hsuper.distributed-static;
          distributed-process = self.haskell.lib.doJailbreak hsuper.distributed-process;
          stm-containers = self.haskell.lib.dontCheck (self.haskell.lib.doJailbreak hsuper.stm-containers);
          rank1dynamic = self.haskell.lib.dontCheck hsuper.rank1dynamic;
          c2hsc = self.haskell.lib.dontCheck hsuper.c2hsc;
        });
  });
}


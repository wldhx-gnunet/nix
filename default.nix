{ nixpkgs, declInput }: let pkgs = import nixpkgs {}; in {
  jobsets = pkgs.runCommand "hydra.json" {} ''
    cat <<EOF
    ${builtins.toXML declInput}
    EOF
    cat > $out <<EOF
    {
        "gnunet": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "src",
            "nixexprpath": "contrib/packages/nix/default.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "src": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git release-18.09", "emailresponsible": false }
            }
        },
        "gnunet-rolling": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "src",
            "nixexprpath": "contrib/packages/nix/default.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "src": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git", "emailresponsible": false }
            }
        },
        "bindings-gnunet": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "src",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "src": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/bindings-gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git release-18.09", "emailresponsible": false }
            }
        },
        "bindings-gnunet-rolling": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "src",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "src": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/bindings-gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git", "emailresponsible": false }
            }
        },
        "gnunet-hs": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "gnunet-hs",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "gnunet-hs": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet-hs.git", "emailresponsible": false },
                "bindings-gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/bindings-gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git release-18.09", "emailresponsible": false }
            }
        },
        "gnunet-hs-rolling": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "gnunet-hs",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "gnunet-hs": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet-hs.git", "emailresponsible": false },
                "bindings-gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/bindings-gnunet.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git", "emailresponsible": false }
            }
        },
        "gnunet-rs": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "gnunet-rs",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "gnunet-rs": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet-rs.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git release-19.03", "emailresponsible": false }
            }
        },
        "gnunet-rs-rolling": {
            "enabled": 1,
            "hidden": false,
            "description": "",
            "nixexprinput": "gnunet-rs",
            "nixexprpath": "release.nix",
            "checkinterval": 300,
            "schedulingshares": 100,
            "enableemail": false,
            "emailoverride": "",
            "keepnr": 3,
            "inputs": {
                "gnunet-nix": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/nix.git", "emailresponsible": false },
                "gnunet": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet.git", "emailresponsible": false },
                "gnunet-rs": { "type": "git", "value": "https://gitlab.com/wldhx-gnunet/gnunet-rs.git", "emailresponsible": false },
                "nixpkgs": { "type": "git", "value": "git://github.com/NixOS/nixpkgs.git", "emailresponsible": false }
            }
        }
    }

    EOF
  '';
}


self: super: {
  gnunet = super.callPackage <gnunet/contrib/packages/nix/gnunet-dev.nix> { };
  haskellPackages = super.haskellPackages.override (oldArgs: {
    overrides =
      self.lib.composeExtensions (oldArgs.overrides or (_: _: {}))
        (hself: hsuper: {
          bindings-gnunet = hsuper.callPackage <bindings-gnunet/bindings-gnunet.nix> { };
          gnunet-hs = hsuper.callPackage <gnunet-hs/gnunet.nix> { };
        });
  });
}

